"""
Tests for the Lambda shell handler
"""


from lambda_shell import handler


def test_execute_command():
    "Test the 'execute_command' function"
    error_output = ["Process errored out",
                    "Process took too long", "", "tr: Illegal byte sequence"]

    valid_commands = [
        "ls -al", "head -c 500 /dev/urandom | tr -dc 'a-zA-Z0-9~!@#$%^&*_-' | fold -w 3 | head -n 1"]
    for command in valid_commands:
        assert handler.execute_command(command) not in error_output

    invalid_commands = ["which asdfghijklasdf", "a"]
    for command in invalid_commands:
        result = handler.execute_command(command)
        assert result in error_output
        assert result == ""

    timeout_commands = ["sleep 20"]
    for command in timeout_commands:
        result = handler.execute_command(command)
        assert result in error_output
        assert result == ""
