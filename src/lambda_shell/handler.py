"""
Handler for processing incoming websocket requests
"""


import json
import os

# # Max message size is 32 KB
# # 1 KB = 1000 Bytes,
# # 1 UTF-8 character is at most 4 bytes
# WEBSOCKET_MESSAGE_SIZE_LIMIT = 32000 / 4


def execute_command(command):
    "Execute a given command in subprocess"
    try:
        output = os.popen(command).read()
        return output
    except Exception as e:
        print(e)
        return "Process errored out"


def shell(event, context):
    "Handle incoming websocket shell request"
    body = json.loads(event.get("body"))
    if body:
        command = body.get("command")
        if command:
            output = execute_command(command)
            body = {
                "message": output,
            }
            response = {
                "statusCode": 200,
                "body": json.dumps(body)
            }
            return response

    body = {
        "message": "No valid command found"
    }
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }

    return response
