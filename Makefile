install:
	pip install -e src

test:
	pytest

lint:
	pylint src/lambda_shell

full-test:
	make lint
	make test